<div id='kiosk6' class='kiosk_check'></div>

<div class='loader'>
	<div>
		<img src="<?= FRONT_ASSETS ?>img/cbma_logo.png">
	</div>
</div>

<a class='home' href="/home/kiosk6"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>

<img id='close' src="<?= FRONT_ASSETS ?>img/x_dark.png">
<section class='slider member_page members'>
	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk6_img2.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>KEN PORTER</h4>
		</div>
		<div class='quotes'>
			<h4>KEN PORTER</h4>
			<p style='text-transform: uppercase;'>“My heart’s desire today is to see growth and success happen not just for me but for those around me, that also look like me. I want to be able to inspire young black men to be great and to move forward despite the fear and risk that comes with entrepreneurship.”</p>
		</div>
	</div>

	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk6_img3.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>Tori Weiston Serdan</h4>
		</div>
		<div class='quotes'>
			<h4>Tori Weiston Serdan</h4>
			<p>"ESPECIALLY IN MY WORK WITH YOUNG BLACK AND LATINO MEN, IT'S ABOUT HELPING THEM UNDERSTAND GENDER NORMS, PATRIARCHY AND TOXIC MASCULINITY, AND THE ROLES THEY PLAY IN THEIR FORMATION AS YOUNG PEOPLE AND AS YOUNG MEN."</p>
		</div>
	</div>

	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk6_img4.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>ANTHONY SMITH</h4>
		</div>
		<div class='quotes'>
			<h4>ANTHONY SMITH</h4>
			<p>"WE MUST CREATE MORE OPPORTUNITIES FOR YOUNG BLACK MEN AND BOYS TO ENGAGE AND BECOME INFORMED ABOUT OUR EFFORTS AND WE MUST FIND MORE RESOURCES TO CONTINUE TO BUILD OUT OUR EFFORTS."</p>
		</div>
	</div>

	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk6_img8.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>Regina Jackson</h4>
		</div>
		<div class='quotes'>
			<h4>Regina Jackson</h4>
			<p style='text-transform: uppercase;'>"Black Male achievement is a long term commitment with twists and turns victories and sacrifices but as the road to success steepens, the support of the network must strengthen. The village must now stretch beyond its borders. We must BE the foundation behind the man."</p>
		</div>
	</div>

	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk6_img5.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>QUAN NELOMS</h4>
		</div>
		<div class='quotes'>
			<h4>QUAN NELOMS</h4>
			<p>"IT IS IMPORTANT FOR YOUNG BLACK MEN TO SEE BLACK MEN IN FRONT OF THEM AS TEACHERS AND ROLE MODELS."</p>
		</div>
	</div>

	<div class='slide color_slide' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk6_img6.jpg');">
		<div class='info'>
			<div class='action'>
				<img src="<?= FRONT_ASSETS ?>img/quote_dark.png">
			</div>
			<h4>MONIQUE LISTON</h4>
		</div>
		<div class='quotes'>
			<h4>MONIQUE LISTON</h4>
			<p>"THE NARRATIVE PIECE IS SO CRUCIALLY IMPORTANT BECAUSE IT'S NOT ONLY WHAT NARRATIVE IS BEING PERPETUATED. IT'S THE IDEA OF WHICH ONES WE PUT VALUE IN, AND WHICH ONES WE ACCEPT AS TRUE."</p>
		</div>
	</div>
</section>

<script type="text/javascript">

	$('.slider').slick({
	    arrows: true,
	    slidesToShow: 1,
	    autoplay: true,
	    focusOnSelect: false,
	    autoplaySpeed: 3000
	  });

	$('.action').click(function(){
		if ( $(this).hasClass('open') ) {
			$('button').show();
			$(this).parents('.slide').children('.quotes').slideUp();
			$(this).children('img').attr('src', '<?= FRONT_ASSETS ?>img/quote_dark.png');
			$(this).removeClass('open');
			$('.info h4').fadeIn();
			$('#close').fadeOut();
			
		}else {
			$('button').hide();
			$(this).parents('.slide').children('.quotes').slideDown();
			$(this).children('img').attr('src', '<?= FRONT_ASSETS ?>img/x_dark.png');
			$(this).addClass('open');
			$('.info h4').fadeOut();
			$('#close').fadeIn();
		}
	});

	$('#close').click(function(){
		$('button').show();		
		$('.quotes').slideUp();
		$('.info h4').fadeIn();
		$('#close').fadeOut();
		$('.action').removeClass('open');
		$('.action').children('img').attr('src', '<?= FRONT_ASSETS ?>img/quote_dark.png');
	});
</script>