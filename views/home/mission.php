<div id='kiosk1' class='kiosk_check'></div>


<div class='loader'>
	<div>
		<img src="<?= FRONT_ASSETS ?>img/cbma_logo.png">
	</div>
</div>

<a class='home' href="/home/kiosk1"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>

<section class='title_holder purp'>
	<h2>10 years commited to helping Black men and boys</h2>
</section>

<section class='text_full'>
	<p>CBMA has spent the past 10 years committed to helping Black men and boys fulfill their potential. CBMA identified several key initiatives to target for impact, all of which are aligned with our mission of narrative change, movement building, and field building. As posed by our CEO Shawn Dove, the question of “what does winning look like for Black men and boys?” drives our work and exemplifies our commitment to advancing this work. Through our signature programs, services, and resources, CBMA is building a legacy for the BMA field.</p>
</section>

<button class='btn purp'>LEARN MORE<i class="fas fa-caret-right"></i></button>


<section class='video'>
	<video src='<?= FRONT_ASSETS ?>img/kiosk2_vid1.mp4' loop autoplay muted></video>
	<i class="fas fa-angle-double-up"></i>
</section>


<script type="text/javascript">
	$('button').click(function(){
		$('.video').slideDown();
	});

	$('.video i').click(function(){
		$('.video').slideUp();
	});
</script>