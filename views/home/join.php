<div id='kiosk6' class='kiosk_check'></div>

<div class='loader'>
	<div>
		<img src="<?= FRONT_ASSETS ?>img/cbma_logo.png">
	</div>
</div>

<a class='home' href="/home/kiosk6"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>

<section class='text'>
	<p>Snap your photo and enter your information to receive it via email, and to automatically be signed up to the CBMA network! </p>
	<div class='action cam'>
		<i class="fas fa-camera"></i>
	</div>
</section>

<section class='background_text' style="background-image: url('<?= FRONT_ASSETS ?>img/kiosk6_img7.jpg');">
</section>


<script type="text/javascript">
	$('.cam').click(function(){
		$('.white').fadeIn();
		setTimeout(function(){
			window.location = "/home/photobooth"
		}, 500);
	});
</script>