<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active">
			<input type="hidden" name="id" value="<?php echo $model->highlight->id; ?>" />
			<input name="token" type="hidden" value="<?php echo get_token();?>" />
			<div class="row">
				<div class="col-md-24">
					<div class="box">
						<h4>Highlights</h4>
						<div class="form-group">
							<label>Highlights Banner</label>
							<p><input type="file" name="image" class="image" /></p>
							<div style="display:inline-block;">
								<?php
									$img_path = "";
									if($model->highlight->image != "" && file_exists(UPLOAD_PATH.'highlights'.DS.$model->highlight->image)){
										$img_path = UPLOAD_URL.'highlights/'.$model->highlight->image;
										?>
										<div class="well well-sm pull-left" style="max-width: 100px;
										max-height: 100px;">
										<img src="<?php echo $img_path; ?>"/>
										<br />
										<a href="<?= ADMIN_URL.'highlights/delete_image/'.$model->highlight->id; ?>?photo=1" onclick="return confirm('Are you sure?');"
										class="btn btn-default btn-xs">Delete</a>
										<input type="hidden" name="image" value="<?=$model->highlight->image?>" />	
										</div>
									<? }
								?>
								<div class="preview-container" style="max-width: 10px;
								max-height: 10px;">
									
								</div>
							</div>
						</div>
						<div class="form-group">
                            <label>Display Order</label>
                            <?php echo $model->form->editorFor("display_order"); ?>
                        </div>
					</div>
					
				</div>
			</div>
		</div>
		
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-save">Save</button>
	</div>
</form>