<?php
if(count($model->supports) > 0) { ?>
	<div class="box box-table">
		<table class="table">
			<thead>
				<tr>
					<th width=2%>Id</th>
					<th width=10%>Support List</th>
					<th width=10%>Phone</th>
					<th width="5%" class="text-center">Edit</th>
          			<th width="5%" class="text-center">Delete</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($model->supports as $obj){  ?>
					<tr>
						<td><a href="<?php echo ADMIN_URL; ?>supports/update/<?php echo $obj->id?>"><?php echo $obj->id;?></a></td>
						<td><a href="<?php echo ADMIN_URL; ?>supports/update/<?php echo $obj->id?>"><?php echo $obj->support_list;?></a></td>
						<td><a href="<?php echo ADMIN_URL; ?>supports/update/<?php echo $obj->id?>"><?php echo $obj->phone;?></a></td>				
						<td class="text-center">
				        	<a class="btn-actions" href="<?= ADMIN_URL ?>supports/update/<?= $obj->id ?>">
				           		<i class="icon-pencil"></i> 
				           	</a>
				        </td>
				        <td class="text-center">
				        	<a class="btn-actions" href="<?= ADMIN_URL ?>supports/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
				           		<i class="icon-cancel-circled"></i> 
				           	</a>
				        </td>
					</tr>
				<? } ?>
			</tbody>
		</table>
	</div>
<? } else { ?>
	<h4>No Support Contact Details. </h4>
<? } ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'supports';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>