<?php

use Twilio\Rest\Client;

class homeController extends siteController {
        function __construct(){
        parent::__construct();
    }    
    public function index(Array $params = []){
        $this->loadView($this->viewData);
    }

    public function kiosk1(Array $params = []){
        $this->loadView($this->viewData);
    }

        public function about(Array $params = []){
            $this->loadView($this->viewData);
        }

        public function turns(Array $params = []){
            $this->loadView($this->viewData);
        }

        public function share(Array $params = []){
            $this->loadView($this->viewData);
        }

    public function kiosk2(Array $params = []){
        $this->loadView($this->viewData);
    }

        public function mission(Array $params = []){
            $this->loadView($this->viewData);
        }

        public function programs(Array $params = []){
            $this->loadView($this->viewData);
        }

        public function impact(Array $params = []){
            $this->loadView($this->viewData);
        }

    public function kiosk3(Array $params = []){
        $this->loadView($this->viewData);
    }

        public function bma(Array $params = []){
            $this->loadView($this->viewData);
        }

        public function events(Array $params = []){
            $this->loadView($this->viewData);
        }

        public function selfie(Array $params = []){
            $this->loadView($this->viewData);
        }

    public function kiosk4(Array $params = []){
        $this->loadView($this->viewData);
    }

        public function narrative(Array $params = []){
            $this->loadView($this->viewData);
        }

        public function influencers(Array $params = []){
            $this->loadView($this->viewData);
        }

        public function poll(Array $params = []){
            $this->loadView($this->viewData);
        }

    public function kiosk5(Array $params = []){
        $this->loadView($this->viewData);
    }

        public function future(Array $params = []){
            $this->loadView($this->viewData);
        }

        public function testimonials(Array $params = []){
            $this->loadView($this->viewData);
        }

        public function decade(Array $params = []){
            $this->loadView($this->viewData);
        }

    public function kiosk6(Array $params = []){
        $this->loadView($this->viewData);
    }

        public function network(Array $params = []){
            $this->loadView($this->viewData);
        }

        public function members(Array $params = []){
            $this->loadView($this->viewData);
        }

        public function join(Array $params = []){
            $this->loadView($this->viewData);
        }

    function photobooth(Array $params = []){
        $this->loadView($this->viewData);
    }
    
    function photobooth2(Array $params = []){
        $this->loadView($this->viewData);
    }

    public function submit_survey(){
        $resp['status'] = false;
        if(isset($_POST) && $_POST != ''){
            $survey_answer = \Model\Survey::loadFromPost();
            if($survey_answer->save()){
                $resp['status'] = true;
            }
        }
        $this->toJson($resp);
    }
   
}