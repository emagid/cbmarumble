<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 11/4/15
 * Time: 2:03 PM
 */

class signinController extends siteController {
    function __construct(){
        parent::__construct();
    }

    public function index(Array $params = [])
    {
        $this->loadView($this->viewData);
    }

    public function index_post()
    {
        \Model\User::login($_POST['username'],$_POST['password']);
        redirect(isset($_POST['redirect_url']) && $_POST['redirect_url']!='/signin'?$_POST['redirect_url']:'/');
    }
}