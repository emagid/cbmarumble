<?php

namespace Model;

class Support_User extends \Emagid\Core\Model {
    static $tablename = "public.support_user";

    public static $fields  =  [
        'support_list_id',
        'user_phone',
    ];

}